<?php 
global $post;
$page404 = get_field('page_404', 'option');

if( $page404 ):

    $post = get_post($page404); setup_postdata($post);
    get_header();

    get_template_part( 'template-parts/content', get_post_type() );

    get_footer();
    
else:

    wp_redirect(home_url(), 301);

endif;
?>

<?php
/**
 * The main template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<?php

	if ( ! is_front_page() ) :
		?>
		<header class="top-padding-regular">

			<!-- Titre -->
			<div class="wrapper bg-banner bg-banner-page-title">
				<i class="h1-like wrapper-medium left-for-desktop is-centered"><?php echo get_bloginfo('name'); ?></i>
				<h1 class="h2-like wrapper-medium left-for-desktop is-centered"><?php the_title(); ?></h1>
			</div>

		</header>
		<?php

	endif;

	if ( have_posts() ) :

	echo '<main class="wrapper above-bg-banner">';
		echo '<div class="wrapper-medium is-centered listing-search">';

		/* Start the Loop */
		while ( have_posts() ) :
			the_post();
			
			/*
				* Include the Post-Type-specific template for the content.
				* If you want to override this in a child theme, then include a file
				* called content-___.php (where ___ is the Post Type name) and that will be used instead.
				*/
			get_template_part( 'template-parts/archive', get_post_type() );

		endwhile;

		echo '</div>';

		// Pagination
		//the_posts_navigation();
		ihag_page_navi();

	echo '</main>';
	
else:

	get_template_part( 'template-parts/content', 'none' );

endif;
?>

<?php
get_footer();

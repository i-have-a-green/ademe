<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<?php 

// Archive title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
	
		echo '<i class="h1-like wrapper-medium left-for-desktop is-centered hide-child">';
			$pt = get_queried_object();
			$pt_label = $pt->labels->singular_name;

			if ($postTypeName) {
				echo $postTypeName;
			} else {
				echo get_bloginfo('name');
			}
		echo '</i>';

		
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered">'. single_cat_title( '', false ) .'</h1>';

		// Archive Description
		if ( get_the_archive_description() ) {
			echo '<div class="entry-content wrapper-medium is-centered top-padding-tiny">'. 	get_the_archive_description() .'</div>';
		}
	echo '</div>';

echo '</header>';

// Archive Content
echo '<main id="archive-content" class="wrapper above-bg-banner">';

	// Load Filters
	get_template_part( 'template-parts/part','taxo' ); 

	// Listing container
	echo '<div id="archive-listing">';

		if ( have_posts() ) :

			$post_type = get_post_type();

			if ($post_type) {
				echo '<div class="listing-'.$post_type.'">';
			} else {
				echo '<div class="listing-default">';
			}

				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/archive', get_post_type() );
				endwhile;
				
			echo '</div>';

			// Pagination
			ihag_page_navi();
				

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;

	echo '</div>';
echo '<main>';
?>

<?php
get_footer();

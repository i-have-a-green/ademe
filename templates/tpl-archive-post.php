<?php
/*
Template Name: Actualités et évènements
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<header class="top-padding-regular">

	<!-- Titre -->
	<div class="wrapper bg-banner bg-banner-page-title">
		<i class="h1-like wrapper-medium left-for-desktop is-centered"><?php echo get_bloginfo('name'); ?></i>
		<h1 class="h2-like wrapper-medium left-for-desktop is-centered"><?php the_title(); ?></h1>
	</div>

</header>

<main id="archive-content" class="wrapper above-bg-banner btm-padding-regular"> 

	<?php get_template_part( 'template-parts/part','taxo' ); ?>

	<div id="archive-listing">

		<!-- Archive part 1 : Event -->
		<section class="listing-event">

			<!-- Evènement -->
			<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
				if($num_page < 2):	
					// get_post();

					$posts = get_posts( array(
						'post_type'			=> 'event',
						'posts_per_page' 	=> -1,
						'meta_key'			=> 'date-start',
						'orderby'			=> 'meta_value_num',
						'order'				=> 'ASC',
						'meta_query' => array(
							array(
								'key' => 'date-start',
								'value' => date('Ymd'),
								'type' => 'DATE',
								'compare' => '>='
							)
							),
					) );
			
					if( $posts ):
						foreach( $posts as $post ):?>					
						<div class="grid-post wrapper-medium">
							<?php                
								setup_postdata( $post );
								get_template_part('template-parts/archive', 'event'); 
							?>
						</div>	
						
						<?php 
						endforeach;
						wp_reset_postdata();
					endif;
				endif;
			?>

		<div id="start-listing"></div>

		</section>

		<!-- Archive part 2 : Actualités -->
		<section class="listing-post" >
			<?php
				// Condition import article
				$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
				$type = "post";
				$args = array(
					'paged' => $num_page,
					'post_type'   => $type,
					'tax_query' => array(
						'relation' => 'OR',
					)
				);
				$my_taxonomies = get_object_taxonomies('post');
				foreach($my_taxonomies as $my_taxonomy){
					$taxonomy = get_taxonomy($my_taxonomy);
					if(isset($_GET[$taxonomy->name])){
						$args['tax_query'][] = array(
							'taxonomy' => $taxonomy->name,
							'field'    => 'slug',
							'terms'    => $_GET[$taxonomy->name],
						);
					}
				}
				
				// Import Article
				query_posts($args);
				global $wp_query; 
				if ( have_posts() ) : while (have_posts()) : the_post();?>
				<div>
					<?php get_template_part('template-parts/archive', "post");?>
				</div>
				<?php endwhile; 
				else:
					get_template_part( 'template-parts/content', 'none' );
				endif;
			?>

		</section>

		<!--  Pagination -->
		<?php ihag_page_navi()?>
		<?php wp_reset_query(); ?>
	
	</div><!-- /#archive-listing -->
	
</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>


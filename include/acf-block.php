<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'ademe',
                'title' => __( 'ADEME', 'ademe' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(
            array(
                'name'				    => 'titre',
                'title'				    => __('Titre'),
                'description'		    => __('Titre h1, h2, h3 ou h4'),
                'placeholder'		    => __('Titre'),
                'render_template'	    => 'template-parts/block/titre.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        ); 

        acf_register_block_type(
            array(
                'name'				    => 'paragraphe',
                'title'				    => __('Paragraphe'),
                'description'		    => __('Bloc texte (éditeur de contenus)'),
                'placeholder'		    => __('Paragraphe'),
                'render_template'	    => 'template-parts/block/paragraphe.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('paragraphe', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'image',
                'title'				    => __('Image'),
                'description'		    => __('Image avec une légende'),
                'placeholder'		    => __('image'),
                'render_template'	    => 'template-parts/block/image.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'format-image',
                'keywords'			    => array('agence', 'légende', 'bloc', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        
        /*
        acf_register_block_type(
            array(
                'name'				    => 'accueil',
                'title'				    => __('Accueil'),
                'description'		    => __('Permet d’afficher la date de création de l article, du texte administrable et d y inclure des images'),
                'placeholder'		    => __('accueil'),
                'render_template'	    => 'template-parts/block/accueil.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'editor-kitchensink',
                'keywords'			    => array('accueil', 'bloc', 'text', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'accueil-actuality',
                'title'				    => __('Accueil-actualité'),
                'description'		    => __('Permet d’afficher la date de l actualité, et image et sa légende (optionnelle), 2 boutons de partage sur les réseaux sociaux, une accroche'),
                'placeholder'		    => __('accueil actualité'),
                'render_template'	    => 'template-parts/block/accueil-actuality.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'editor-kitchensink',
                'keywords'			    => array('accueil', 'actualité', 'partage', 'bloc', 'text', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        */


        acf_register_block_type(
            array(
                'name'				    => 'download-document',
                'title'				    => __('Documents à télécharger'),
                'description'		    => __('Permet d’afficher un titre, puis des documents téléchargeables par l utilisateur avec : un nom qui est le lien de téléchargement, une description, une image optionnelle'),
                'placeholder'		    => __('Documents à télécharger'),
                'render_template'	    => 'template-parts/block/download_document.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'download',
                'keywords'			    => array('document', 'télécharger', 'text', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        

        /* Ancien design - plus d'actualité
        acf_register_block_type(
            array(
                'name'				    => 'logo-partners',
                'title'				    => __('Logo-partenaires'),
                'description'		    => __('Permet d’afficher le logo des organismes partenaires du projet. Celui-ci peut renvoyer sur la page du partenaire (optionnel)'),
                'placeholder'		    => __('Logo-partenaires'),
                'render_template'	    => 'template-parts/block/logo-partenaires.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'buddicons-buddypress-logo',
                'keywords'			    => array('logo', 'lien', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        */

        
        acf_register_block_type(
            array(
                'name'				    => 'contact',
                'title'				    => __('Contact'),
                'description'		    => __('Permet d’afficher le nom, le prenom et le mail du référent du projet. Cliquer sur le mail ouvre un mail dans le navigateur par défaut.'),
                'placeholder'		    => __('Contact'),
                'render_template'	    => 'template-parts/block/contact.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'email-alt',
                'keywords'			    => array('nom', 'prénom', 'mail', 'contact'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        /*
                acf_register_block_type(
            array(
                'name'				    => 'accueil-ami',
                'title'				    => __('Accueil AMI'),
                'description'		    => __('Permet d’afficher une image, du texte administrable et trois bouton renvoyant vers agenda AMI, ressources AMI, cartographie AMI'),
                'placeholder'		    => __('Accueil AMI'),
                'render_template'	    => 'template-parts/block/accueil-ami.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'editor-kitchensink',
                'keywords'			    => array('image', 'text', 'menu', 'AMI', 'accueil'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        */

        acf_register_block_type(
            array(
                'name'				    => 'accueil-ademe',
                'title'				    => __('Accueil ADEME'),
                'description'		    => __('Permet d’afficher une image, sur laquelle s affichent 3 textes'),
                'placeholder'		    => __('Accueil ADEME'),
                'render_template'	    => 'template-parts/block/accueil-ademe.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'editor-kitchensink',
                'keywords'			    => array('image', 'text', 'ademe', 'accueil'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'presentation-ademe',
                'title'				    => __('Présentation ADEME'),
                'description'		    => __('Permet d’afficher un titre, du texte, les principales vocations du site et un bouton-lien'),
                'placeholder'		    => __('Présentation ADEME'),
                'render_template'	    => 'template-parts/block/presentation-ademe.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'editor-kitchensink',
                'keywords'			    => array('titre', 'text', 'ademe', 'presentation'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'faq',
                'title'				    => __('FAQ'),
                'description'		    => __('Permet d’afficher la taxonomy en titre et des onglets déroulants questions/réponses'),
                'placeholder'		    => __('FAQ'),
                'render_template'	    => 'template-parts/block/faq.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'flag',
                'keywords'			    => array('Titre', 'taxonomy', 'faq', 'questions'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'sous-menu',
                'title'				    => __('Sous-menu'),
                'description'		    => __('Menu avec des liens personnalisés'),
                'placeholder'		    => __('Sous-menu'),
                'render_template'	    => 'template-parts/block/sous-menu.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('menu', 'titre', 'image', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'accueil-ami',
                'title'				    => __('Sous-menu AMI'),
                'description'		    => __('Menu avec des liens les sites AMI'),
                'placeholder'		    => __('Sous-menu AMI'),
                'render_template'	    => 'template-parts/block/accueil-ami.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('image', 'text', 'menu', 'AMI', 'accueil'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );    

        acf_register_block_type(
            array(
                'name'				    => 'lien',
                'title'				    => __('Liens'),
                'description'		    => __('Permet d’afficher un titre, puis des liens cliquables vers d autres pages'),
                'placeholder'		    => __('Documents à télécharger'),
                'render_template'	    => 'template-parts/block/lien.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'download',
                'keywords'			    => array('titre', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'fiche-partners',
                'title'				    => __('Fiche-partenaires'),
                'description'		    => __('Permet d’afficher le logo et le nom des organismes partenaires du projet. L utilisateur peut administrer un texte de description et renseigner un lien vers le site du partenaire.'),
                'placeholder'		    => __('Fiche-partenaires'),
                'render_template'	    => 'template-parts/block/fiche-partenaires.php',
                'category'			    => 'ademe',
                'mode'                  => 'edit',
                'icon'				    => 'buddicons-buddypress-logo',
                'keywords'			    => array('logo', 'lien', 'text', 'nom', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

    }
}
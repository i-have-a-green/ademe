<?php
 
 
add_action('wp_enqueue_scripts', 'site_scripts', 999);
function site_scripts()
{
    global $wp_styles;
    wp_enqueue_style('styles', get_template_directory_uri() . '/style.css');

    if(get_page_template_slug() == 'templates/tpl-archive-project.php' ){
        wp_enqueue_style('leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css');
        wp_enqueue_script('leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js');
    }

    wp_dequeue_style( 'wp-block-library' );

    wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', false, false, 'all');
    wp_enqueue_script('script');
    wp_localize_script('script', 'resturl', get_bloginfo('url') . '/wp-json/ihag/');

}

<?php
/**
 * Block Name: Bloc Accueil ADEME
 */
 ?>

<section class="wp-block blk-home-ademe wrapper">

<?php
$background_image = get_field('background_image');

if ( !$background_image ):
	echo '<em>Renseigner le contenu</em>';
	
else :

	echo '<div class="wrapper">';

		echo '<div class="wrapper-large is-centered layout-content">';

			$title = get_field('welcome_text');
			$content = get_field('describe_text');
			$logo = get_field('superimposed_image');

			if ($title || $content) {

				echo '<h1 class="no-margin">';

				if(!empty($title)):
					echo '<span class="txt-title">'. $title .'</span><br>';
				endif;

				if(!empty($content)):
					echo '<span class="txt-content bg-banner bg-banner-accueil">'. $content .'</span>';
				endif;

				echo '</h1>';

			}
			
			if(!empty(get_field('superimposed_image'))):
				$superimposed_image = get_field('superimposed_image');
				$size = 'logo-blk-home';
				echo '<span class="content-img above-bg-banner">';
				echo wp_get_attachment_image($superimposed_image, $size); 	
				echo '</span>';	
			endif;	
		
		echo '</div>';
	echo '</div>'; //.wrapper

	if(!empty ($background_image)):
		$size = 'fullscreen';
		echo wp_get_attachment_image($background_image, $size, "", array( "class" => "bg-image" ) ); 
	endif;
 
endif; ?>
</section>

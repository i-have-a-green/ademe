<?php
/**
 * Block Name: Paragraphe
 */

$texte = get_field('text');

if ( empty($texte) ) :

	echo "<em>Renseigner le paragraphe</em>";

else :

	echo '<div class="wp-block wrapper btm-padding-tiny">';
		echo '<div class="wrapper-medium is-centered entry-content">'. $texte .'</div>';
	echo '</div>';

endif;
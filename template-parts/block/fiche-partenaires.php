<?php
/**
 * Block Name: Fiche - partenaires
 */
 ?>

<!-- wrapper -->
<div class="wp-block blk-partner wrapper">

<?php 
// Check if rows exist.
if ( !have_rows('plug') ):

echo '<em>Renseigner le block "fiche partenaire"</em>';

else :
            
echo '<ul class="wrapper-large is-centered listing-partner">';

    // Loop through rows.
    while( have_rows('plug') ) : the_row();

        // Load sub field value.
        $logo = get_sub_field("logo");
        $title = get_sub_field("title");
        $text = get_sub_field("text");
        $link = get_sub_field("link");
        
        if (!$logo || !$title || !$text || !$link ):
            echo '<em>'; _e("Renseigner les informations du partenaire", "ademe"); echo '</em>';
        else:

            echo '<li class="partner-card">';
        
            if ($logo):
                echo '<div class="img-container">'. wp_get_attachment_image( $logo, 'block-donwload') .'</div>';
            endif;

            if ($title):
                echo '<h2 class="h3-like">'. $title .'</h2>';
            endif;

            if ($text):
                echo '<div class="entry-content">'. $text .'</div>';
            endif;

            if ($link):
                echo '<a class="link-default" href="'. $link .'" title="'; _e("Consulter le lien ", "ademe"); echo $link .'" >'.$link.'</a>';
            endif;

            echo '</li>';
        
        endif; 

    // End loop.
    endwhile;

echo '</ul>';

endif;?>
    
</div><!-- /wrapper -->
	


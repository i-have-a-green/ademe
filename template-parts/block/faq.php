<?php
/**
 * Block Name: Bloc FAQ
 */
 ?>

<section class="wp-block blk-faq wrapper btm-padding-regular">

<?php
$ami = get_field('ami_faq');

if ( empty($ami) ):

	echo '<em>Renseigner le bloc</em>';

else :

	// Title
	echo '<h2 class="h3-like wrapper-medium is-centered '. ihag_ami_color_class($ami, 'color1') .' ">'. get_blog_details($ami)->blogname .'</h2>';

	if( have_rows('questions_answers') ):

		// Loop through rows.
		while( have_rows('questions_answers') ) : the_row();
		
			$question = get_sub_field('question');
			$answer = get_sub_field('answer');
			if(!empty ($question && $answer) ):
			
				echo '<details class="wrapper-medium is-centered">';
					echo '<summary>'. $question .'</summary>';
					echo '<div class="entry-content">'. $answer.'</div>';
				echo '</details>';

			endif;

		endwhile;

	endif;

endif; 
?>

</section>

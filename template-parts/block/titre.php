<?php
/**
 * Block Name: Titre
 */

$title = get_field('title');
$html = get_field('level_title');

if ( empty($title) ):

	echo "<em>Renseigner le titre</em>";

else :

	if ($html === 'H1' | $html === 'H2' ) {
		// H1 & H2 titles have specific css class (tex-align)
		echo "<". $html ." class='left-for-desktop'>". $title ."</". $html .">";
	} else {
		echo "<". $html .">". $title ."</". $html .">";
	}

endif; 
?>



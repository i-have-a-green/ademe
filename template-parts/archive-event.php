<?php
/**
 * Template part for displaying page archive-event in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php 
$ami_color = get_field('ami', ihag_get_term($post, "ami"));
?>

<article class="event-card">

	<!-- Date -->	
	<p class="event-date text-small uppercase no-margin">
		<?php  
		// Event Date
		echo '<time  datetime="'. date_i18n($format = 'Y-m-d', strtotime(get_field('date-start'))) .'">';
		echo date_i18n( get_option('date_format'), strtotime(get_field('date-start')));

		if(get_field('need_second_date') == true):
			if(!empty(get_field('second_date'))):
				_e(' au ', 'ademe');
				echo date_i18n( get_option('date_format'), strtotime(get_field('second_date')));
			endif;
		endif;
		echo '</time>';
		?>
	</p>

	<!-- Title -->
	<div class="event-title">
		<a class="link-color <?php echo ihag_ami_color_class($ami_color, 'color1'); ?>" href="<?php the_permalink();?>" title="">
		<h3 class="no-margin <?php echo ihag_ami_color_class($ami_color, 'color1'); ?>"><?php the_title();?></h3>
		</a>
		<!--<p class="no-margin">Subtitle</p>-->
	</div>

	<!-- Button -->
	<a class="button-brd" href="<?php the_permalink();?>" title="<?php _e('Lire l\'évènement ', 'ademe'); the_title();?>">
		<p class="no-margin"><?php _e('Lire', 'ademe'); ?></p>
	</a>

</article>



<?php
/**
 * Template part for displaying page archive-post in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php 
$ami_color = get_field('ami', ihag_get_term($post, "ami"));
?>

<article class="post-card">

	<!-- date -->
	<time class="uppercase" datetime="<?php echo get_the_date('Y-m-d');?>">
		<?php echo date_i18n( get_option('date_format'), strtotime(get_the_date('Ymd')));?>
	</time>

	<!-- Title -->
	<a class="link-color <?php echo ihag_ami_color_class($ami_color, 'color1'); ?>" href="<?php the_permalink();?>" >
		<h3 class="no-margin <?php echo ihag_ami_color_class($ami_color, 'color1'); ?>"><?php the_title();?></h3>
	</a>

	<!-- Thumbnail -->
	<a class="post-thumb" href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php 
		$size = 'archive-post';
		//ihag_the_post_thumbnail($size); No fallback-image needed
		the_post_thumbnail( 'archive-post', ['class' => 'img-responsive'] );
		?>
	</a>

	<!-- Excerpt -->
	<?php the_excerpt();?>

	<a class="button-brd" href="<?php the_permalink();?>" title="<?php _e('Lire l\'évènement ', 'ademe'); the_title();?>">
		<p class="no-margin"><?php _e('Lire', 'ademe'); ?></p>
	</a>

</article>

<?php
/**
 * Template part for displaying page archive-ressource in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php 
$ami_color = get_field('ami', ihag_get_term($post, "ami"));
?>

<article class="ressource-card">

	<!-- Thumbnail -->
	<a class="img-container link-discrete right" href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php ihag_the_post_thumbnail('archive-ressource', $attr = array( "class" => "img-in-link img-responsive" ));?>
	</a>

	<div class="txt-container">

		<!-- Title -->
		<a class="link-color <?php echo ihag_ami_color_class($ami_color, 'color1'); ?> " href="<?php the_permalink();?>">
			<h2 class="h3-like no-margin <?php echo ihag_ami_color_class($ami_color, 'color1'); ?>"><?php the_title();?></h2>
		</a>

		<?php 
		if(get_field('sub-head')):
			echo '<p class="h4-like '. ihag_ami_color_class($ami_color, 'color2') .' no-margin">'. get_field('sub-head') .'</p>';
		endif;

		// AMI
		$ami = ihag_get_term($post, 'ami') ;
		//var_dump($ami);
		if ($ami) {
			echo '<div class="body-like button-like-brd uppercase"><p class="btn-tag">'. $ami->name .'</p></div>';
		}
		?>

	</div>

</article>
